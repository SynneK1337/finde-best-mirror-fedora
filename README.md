# Fedora Fastest Mirror Finder
## Simple utility that searches for fastest mirror for Fedora Linux
## Dependencies:
- Library Web Qt
- qt-base
- qt5-qtbase-devel

## Supported repositories
| Repository | Repo Detail                 |
|------------|-----------------------------|
| Fedora     | Main Repository             |
| RPMFusion  | RpmFusion Free and non-free |

## Usage
### Makefile generation
```
cp * /etc/yum.repos.d
cd /etc/yum.repos.d
qmake-qt5
```

### Compilation
```make```

### Run
```
chmod +x ./MirroFinder
sudo ./MirroFinder
```

**This process can take a while, it tests around 100 Fedora mirrors and 40 RPMFusion mirrors**

### Contact
[receive_mail_gitlab@riseup.net](mailto:receive_mail_gitlab@riseup.net)